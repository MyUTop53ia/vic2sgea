# Victoria 2 Savegame Economy Analyzer

Based on old [Nashetovich's code](http://oldforum.paradoxplaza.com/forum/showthread.php?715468). 
Modified by [Aerkrlov](https://github.com/aekrylov/vic2_economy_analyzer/releases) to work with Java 1.8
Modified by MyUTop53ia to track data over time, extend the number of datasets, and provide a line graph for graphical 
displays.


## Usage

1. Install [Java Runtime 8 or newer](https://java.com/download/) if it's not installed
2. Download jar and bat file from [Victoria 2.zip](https://bitbucket.org/MyUTop53ia/vic2sgea/downloads/) into one folder
3. Run bat file. You can also run jar file directly, but this can lead to "out of memory" error. 
4. Specify savegame path (also you need to specify game install and mod paths for correct localisation) and press Load. 

Double click on a table row (or single click on a chart item) opens detailed info window.


## Troubleshooting

- **Can't load large late game file**

    `-Xmx` part in the .bat file is responsible for max memory usage.
    Change `-Xmx1024m` to bigger value, e.g. `-Xmx1500m` (should work for most systems).
    
    Please note that setting this value too big can cause errors on some systems.

- **I want to see if this works, but I don't have any Victoria 2 save files**
    
    The downloads repository contains several compressed save files that can be used for evaluation purposes.