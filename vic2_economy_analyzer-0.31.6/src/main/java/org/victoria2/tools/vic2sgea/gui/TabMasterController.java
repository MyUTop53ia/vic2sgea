package org.victoria2.tools.vic2sgea.gui;
/**
 * 
 */
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.geometry.*;
import org.victoria2.tools.vic2sgea.main.*;
import org.controlsfx.control.RangeSlider;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
public class TabMasterController extends BaseController implements Initializable {
	@FXML 
	TabPane tPane;
	@FXML
	Tab chartTab;
	@FXML
	Tab tableTab;
	
	private WindowController windowController;
	@SuppressWarnings("restriction")
	@Override
    public void initialize(URL arg0, ResourceBundle arg1) {
		try{
		tPane.setSide(Side.LEFT);
		//tableTab = new Tab();
		FXMLLoader windowLoader = new FXMLLoader(getClass().getResource("/gui/Window.fxml"));
		tableTab.setContent((Node) windowLoader.load());
		windowController = windowLoader.getController();
		}catch (Exception e) {
            e.printStackTrace();
            errorAlert(e, "Exception while loading savegame");
		}
    }
		public void setProductListController(ProductListController productListController)
		{
			windowController.setProductListController(productListController);
		}
		/**
		 * Activates and sets up the chart tab. There is an fxml, but most setup is done through java 
		 * @param reports
		 */
		public void activateChartTab(ArrayList<Report> reports)
		{
			chartTab.setDisable(false);
			
			//Chart setup
			ChartsController control = new ChartsController(reports);
			LineChart chart = control.addLineChart(new NumberAxis("Year",reports.get(0).getYear(),reports.get(reports.size()-1).getYear(),1.0),new NumberAxis("GDP",0,100000,50000),"World GDP");
			
			//container panes setup
			BorderPane border = new BorderPane();
			FlowPane treePane = new FlowPane();
			
			//Slider setup
			RangeSlider slider = new RangeSlider(reports.get(0).getYear(),reports.get(reports.size()-1).getYear(),reports.get(0).getYear(),reports.get(reports.size()-1).getYear());
			slider.setMajorTickUnit(1.0);
			
			//Country checkbox tree setup
			CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("World");
			root.selectedProperty().addListener((observable, oldValue, newValue) ->
			{
				if (newValue == true)
				{
					control.addCountryToLineChart(chart, "TOT");	
				}
				else if (newValue == false)
				{
					control.removeCountryFromLineChart(chart, "TOT");
				}
			});
			root.setIndependent(true);
			root.setExpanded(true);
			TreeView countryTree = new TreeView(root);
			ArrayList<Country> countries = new ArrayList<Country>();
			countryTree.setCellFactory(CheckBoxTreeCell.<String>forTreeView());
			String tagString = "";
			for (Report report:reports)
			{
				for (Country country: report.getCountryList())
				{
					if (tagString.indexOf(country.getTag()) == -1 && !country.getTag().equals("TOT"))
					{
						countries.add(country);
						tagString += " "+country.getTag()+" ";
					}
				}
			}
			
			countries.sort((ob1, ob2) -> ob1.getOfficialName().compareTo(ob2.getOfficialName()));
			for (Country country: countries)
			{
					CheckBoxTreeTagItem checkBoxTreeItem = new CheckBoxTreeTagItem(country.getOfficialName(),country.getTag());
					root.getChildren().add(checkBoxTreeItem);
					checkBoxTreeItem.selectedProperty().addListener((observable, oldValue, newValue) ->
					{
						if (newValue == true)
						{
							control.addCountryToLineChart(chart, checkBoxTreeItem.getTag());	
						}
						else if (newValue == false)
						{
							control.removeCountryFromLineChart(chart, checkBoxTreeItem.getTag());
						}
					});
				}
			
			
			countryTree.setShowRoot(true);
			
			//Data checkboxtree setup
			CheckBoxTreeItem<String> rootItem = new CheckBoxTreeItem<String>("View Source Files");
			rootItem.setExpanded(true);                  
			ArrayList<String> availableData = new ArrayList<String>();
			availableData.add("GDP");
			availableData.add("GDP per Capita");
			availableData.add("GDP Percent");
			availableData.add("GDP Growth Rate");
			availableData.add("Population");
			availableData.add("Factory Workforce");
			availableData.add("RGO Workforce");
			availableData.add("Gold Income");
			availableData.add("GDP Place");
			availableData.add("RGO Unemployment Rate");
			availableData.add("Factory Unemployment Rate");
			availableData.add("Size");
			availableData.add("Consumption");
			availableData.add("Exports (Absolute)");
			availableData.add("Imports (Absolute)");
			availableData.add("Employment");
			availableData.add("Balance of Trade (Absolute)");
			availableData.add("Balance of Trade (Relative)");
			availableData.add("Balance of Trade (Percent GDP)");
			availableData.add("Exports (Percent GDP)");
			availableData.add("Imports (Percent GDP)");
			availableData.add("Population Growth Rate");
			availableData.add("Global GDP Growth Contribution");
			availableData.add("Global Population Growth Contribution");
			availableData.add("GDP per Capita Growth Rate");
			availableData.sort((ob1,ob2) -> ob1.compareTo(ob2));
			
			TreeView dataTree = new TreeView(rootItem);  
			dataTree.setCellFactory(CheckBoxTreeCell.<String>forTreeView());    
	        for (int i = 0; i < availableData.size(); i++) {
	             CheckBoxTreeItem<String> checkBoxTreeItem = 
	                new CheckBoxTreeItem<String>(availableData.get(i));
	                    rootItem.getChildren().add(checkBoxTreeItem);     
	                    checkBoxTreeItem.selectedProperty().addListener((observable, oldValue, newValue) ->
						{
							if (newValue == true)
							{
								control.setLineChartSource(chart, checkBoxTreeItem.getValue(),false);
							}
							else if (newValue == false)
							{
								control.setLineChartSource(chart, "",true);
								for (TreeItem<String> cbtti: root.getChildren())
								{
									((CheckBoxTreeTagItem)cbtti).setSelected(false);
								}
							}
						});
	        }
	                       
	        dataTree.setRoot(rootItem);
	        dataTree.setShowRoot(true);
	       dataTree.setMaxHeight(150);
	       treePane.setHgap(20);
	        treePane.getChildren().addAll(dataTree,countryTree);// (dataTree);
			//treePane.getChildren().add(countryTree);
	       
	       
	        treePane.setMaxWidth(100);
	       // countryTree.setMaxHeight(400);
			border.setLeft(treePane);
			//Pane pane = new Pane();
			//pane.getChildren().add(chart);
			//chart.setStyle("-fx-background-color: #0000FF;");
			
			//container setup
			StackPane stack = new StackPane();
			stack.getChildren().addAll(chart,slider);
			border.setCenter(stack);
			
			//Final Chart setup
			chart.setPadding(Insets.EMPTY);
			chart.setMaxWidth(966);
			chart.setMinWidth(966);
			
			
			chartTab.setContent(border);
			
			//slider setup
			slider.setMaxWidth(884.0);
			slider.setShowTickMarks(false);
			slider.setShowTickLabels(true);
			slider.setBlockIncrement(1);
			//slider.setLayoutX(83);
			slider.setTranslateX(34);
			slider.setTranslateY(272);
			
			 slider.lowValueProperty().addListener((observable, oldValue, newValue) -> {
		           if (oldValue.intValue() > newValue.intValue())
		           {
		        	   control.addYearToLineChart(chart, newValue.intValue());
		        	   ((NumberAxis)chart.getXAxis()).setLowerBound(newValue.intValue());
		           }
		           else if (oldValue.intValue() < newValue.intValue())
		           {
		        	   control.removeYearFromLineChart(chart, oldValue.intValue());
		        	   ((NumberAxis)chart.getXAxis()).setLowerBound(newValue.intValue());
		           }
		        });
			 slider.highValueProperty().addListener((observable, oldValue, newValue) -> {
				 if (oldValue.intValue() < newValue.intValue())
		           {
					 control.addYearToLineChart(chart, newValue.intValue());
					 ((NumberAxis)chart.getXAxis()).setUpperBound(newValue.intValue());
		           }
				 else if (oldValue.intValue() != newValue.intValue())
				 {
					 control.removeYearFromLineChart(chart, oldValue.intValue());
					 ((NumberAxis)chart.getXAxis()).setUpperBound(newValue.intValue());
				 }
		        });
		
			/* Used for debugging
			 slider.setOnScroll(new EventHandler<ScrollEvent>() {
		            @Override
		            public void handle(ScrollEvent ev) {
		            	//System.out.println(ev.getX());
		            	if (ev.getDeltaY() < 0)
		            	{
		            		//slider.setMaxWidth(slider.getMaxWidth()/1.01);
		            		 slider.setTranslateY(slider.getTranslateY()-1);
			            	 System.out.println(" layout "+slider.getTranslateY());
		            	}
		            	else
		            	{
		            		 slider.setTranslateY(slider.getTranslateY()+1);
			            	 System.out.println(" layout "+slider.getTranslateY());
		            	 //slider.setMaxWidth(slider.getMaxWidth()*1.01);
		            	}
		            	 System.out.println(" max "+slider.getTranslateY());
		                ev.consume();
		            }
		        });
		        */
		}
		
	    private static void errorAlert(Throwable e, String text) {
	        Platform.runLater(() -> {
	            Alert alert = new Alert(Alert.AlertType.ERROR);
	            alert.setContentText(text);

	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            e.printStackTrace(pw);
	            String exceptionText = sw.toString();

	            Label label = new Label("The exception stacktrace was:");

	            TextArea textArea = new TextArea(exceptionText);
	            textArea.setEditable(false);
	            textArea.setWrapText(true);

	            textArea.setMaxWidth(Double.MAX_VALUE);
	            textArea.setMaxHeight(Double.MAX_VALUE);
	            GridPane.setVgrow(textArea, Priority.ALWAYS);
	            GridPane.setHgrow(textArea, Priority.ALWAYS);

	            GridPane expContent = new GridPane();
	            expContent.setMaxWidth(Double.MAX_VALUE);
	            expContent.add(label, 0, 0);
	            expContent.add(textArea, 0, 1);

	            // Set expandable Exception into the dialog pane.
	            alert.getDialogPane().setExpandableContent(expContent);

	            alert.show();
	        });
	    }
}
class CheckBoxTreeTagItem extends CheckBoxTreeItem
{
	private String tag;
	public CheckBoxTreeTagItem(String officialName, String tag)
	{
		super(officialName);
		this.tag = tag;
	}
	public String getTag()
	{
		return tag;
	}
}