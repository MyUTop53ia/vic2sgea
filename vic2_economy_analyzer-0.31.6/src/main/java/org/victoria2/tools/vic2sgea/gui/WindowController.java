package org.victoria2.tools.vic2sgea.gui;

import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import org.victoria2.tools.vic2sgea.main.*;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * @author nashet
 * @extender Nathaniel Smith
 */
public class WindowController extends BaseController implements Initializable {

    @FXML
    Button btnLoad;
    @FXML
    Button btnGoods;
    @FXML
    ComboBox cbYear;
    @FXML
    Button btnBrowseSave;
    @FXML
    Button btnBrowseLocal;
    @FXML
    Button btnBrowseMod;
    @FXML
    public Label lblStartDate;
    @FXML
    public Label lblCurrentDate;
    @FXML
    public Label lblPlayer;
    @FXML
    public Label lblPopCount;
    @FXML
    TableView<Country> mainTable;
    @FXML
    TextField tfSaveGame;
    @FXML
    TextField tfLocalization;
    @FXML
    TextField tfModPath;

    @FXML
    ProgressIndicator piLoad;

    private static final ObservableList<Country> countryTableContent = FXCollections.observableArrayList();

    private ProductListController productListController;

    @FXML
    public TableColumn<Country, ImageView> colImage;
    @FXML
    public TableColumn<Country, String> colCountry;
    @FXML
    public TableColumn<Country, Long> colPopulation;
    @FXML
    TableColumn<Country, Float> colConsumption;
    @FXML
    TableColumn<Country, Float> colActualSupply;
    @FXML
    TableColumn<Country, Float> colGdp;
    @FXML
    TableColumn<Country, Float> colGDPPer;
    @FXML
    TableColumn<Country, Integer> colGDPPlace;
    @FXML
    TableColumn<Country, Float> colGDPPart;
    @FXML
    TableColumn<Country, Long> colGoldIncome;
    @FXML
    TableColumn<Country, Long> colWorkForceRgo;
    @FXML
    TableColumn<Country, Long> colWorkForceFactory;
    @FXML
    TableColumn<Country, Long> colEmployment;
    @FXML
    TableColumn<Country, Float> colExport;
    @FXML
    TableColumn<Country, Float> colImport;
    @FXML
    TableColumn<Country, Float> colUnemploymentRate;
    @FXML
    TableColumn<Country, Float> colUnemploymentRateFactory;
    
    //Beginning time dependent columns
    @FXML
    TableColumn<Country,Float> colGDPGrowthRate;
    @FXML
    TableColumn<Country,Float> colPopulationGrowth;
    @FXML
    TableColumn<Country,Float> colGDPPerCapitaGrowthRate;
    @FXML
    TableColumn<Country,Float> colConsumptionGrowth;
    @FXML
    TableColumn<Country,Float> colExportsGrowth;
    @FXML
    TableColumn<Country,Float> colImportsGrowth;
	
    private void fillMainTable() {
        countryTableContent.clear();
        countryTableContent.addAll(reports.get(index).getCountryList());
        //Add any time-dependent (rate) columns to this 
        if (reports.size() >= 2)
        {
        	if (index >= 1)
        	{
        		colGDPGrowthRate.setVisible(true);
        		colPopulationGrowth.setVisible(true);
        		colGDPPerCapitaGrowthRate.setVisible(true);
        		colConsumptionGrowth.setVisible(true);
        		colExportsGrowth.setVisible(true);
        		colImportsGrowth.setVisible(true);
        	}
        }
        
        mainTable.setItems(countryTableContent);
    }
    private WindowController self;
    //index controls which report's data is displayed
    private int index = 0;
    private ArrayList<Report> reports = new ArrayList<Report>();
    private List<File> files;
    private boolean autoChanged = false;
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        //add row click handler
        mainTable.setRowFactory(new TableRowDoubleClickFactory<>(country -> Main.showCountry(reports.get(index), country)));
        colImage.setCellValueFactory(features -> {
            String tag = features.getValue().getTag();
            URL url = getClass().getResource("/flags/" + tag + ".png");
            if (url == null)
                return null;

            Image image = new Image(url.toString());
            ImageView iv = new ImageView(image);
            iv.setPreserveRatio(true);
            iv.setFitHeight(20);
            iv.getStyleClass().add("flag");
            return new SimpleObjectProperty<>(iv);
        });

        setFactory(colCountry, Country::getOfficialName);
        setFactory(colPopulation, Country::getPopulation);
        setFactory(colActualSupply, Country::getSold);
        setFactory(colGdp, Country::getGdp);
        setFactory(colConsumption, Country::getBought);
        setFactory(colGDPPer, Country::getGdpPerCapita);
        setFactory(colGDPPlace, Country::getGDPPlace);
        setFactory(colGDPPart, Country::getGDPPart);
        setFactory(colGoldIncome, Country::getGoldIncome);

        setFactory(colWorkForceRgo, Country::getWorkforceRgo);
        setFactory(colWorkForceFactory, Country::getWorkforceFactory);
        setFactory(colEmployment, Country::getEmployment);

        setFactory(colExport, Country::getExported);
        setFactory(colImport, Country::getImported);

        setFactory(colUnemploymentRate, Country::getUnemploymentRateRgo);
        setFactory(colUnemploymentRateFactory, Country::getUnemploymentRateFactory);
        
        setFactory(colGDPGrowthRate, Country::getGDPGrowth);
        setFactory(colPopulationGrowth, Country::getPopulationGrowth);
        setFactory(colGDPPerCapitaGrowthRate,Country::getGDPPerCapitaGrowth);
        setFactory(colConsumptionGrowth, Country::getConsumptionGrowth);
        setFactory(colImportsGrowth,Country::getImportsGrowth);
        setFactory(colExportsGrowth,Country::getExportsGrowth);
        
        
        setCellFactory(colPopulation, new KmgConverter<>());
        setCellFactory(colActualSupply, new KmgConverter<>());
        setCellFactory(colGdp, new KmgConverter<>());
        setCellFactory(colGDPPart, new PercentageConverter());
        setCellFactory(colGDPPer, new NiceFloatConverter());
        setCellFactory(colWorkForceRgo, new KmgConverter<>());
        setCellFactory(colWorkForceFactory, new KmgConverter<>());
        setCellFactory(colEmployment, new KmgConverter<>());
        setCellFactory(colUnemploymentRate, new PercentageConverter());
        setCellFactory(colUnemploymentRateFactory, new PercentageConverter());
      
        
        setCellFactory(colGDPGrowthRate, new PercentageConverter());
        setCellFactory(colGDPPerCapitaGrowthRate, new PercentageConverter());
        setCellFactory(colPopulationGrowth, new PercentageConverter());
        setCellFactory(colConsumptionGrowth, new PercentageConverter());
        setCellFactory(colExportsGrowth, new PercentageConverter());
        setCellFactory(colImportsGrowth, new PercentageConverter());
        
        
        colConsumption.setVisible(false);
        colActualSupply.setVisible(false);
        colWorkForceRgo.setVisible(false);
        colWorkForceFactory.setVisible(false);
        colEmployment.setVisible(false);
        colExport.setVisible(false);
        colImport.setVisible(false);
        colUnemploymentRate.setVisible(false);
        
	    colGDPGrowthRate.setVisible(false);
	    colPopulationGrowth.setVisible(false);
	    colGDPPerCapitaGrowthRate.setVisible(false);
	    colConsumptionGrowth.setVisible(false);
	    colImportsGrowth.setVisible(false);
	    colExportsGrowth.setVisible(false);
	    
        /*try {
            Config config = new Config();
        } catch (IOException e) {
            e.printStackTrace();
            errorAlert(e, "Couldn't load config");
        }*/
        PathKeeper.checkPaths();
        tfLocalization.setText(PathKeeper.LOCALISATION_PATH);
        autoChanged = true;
        tfSaveGame.setText(PathKeeper.SAVE_PATH);
        tfModPath.setText(PathKeeper.MOD_PATH);
        //Necessary for differentiating between user changes to tfSaveGame (which should reset everything) 
        //and changes by other parts of the program (which shouldn't)
        tfSaveGame.textProperty().addListener((observable, oldValue, newValue) -> {
            if (autoChanged)
            {
            	autoChanged = false;
            }
            else
            {
            	files = new ArrayList<File>();
            	files.add(new File(newValue));
            	index = 0;
            	reports.clear();
            }
        });
        lblPlayer.setOnMouseClicked(e -> {
            if (reports != null) {
                Main.showCountry(reports.get(index), reports.get(index).getPlayerCountry());
            }

        });

        self = this;

    }
    /**
     * Changes the displayed data depending on the combo box selection
     * @param event
     */
    public final void onYear(ActionEvent event)
    {
    	try
    	{
    	index = reports.size()-cbYear.getSelectionModel().getSelectedIndex()-1;
    	int fileIndex = cbYear.getSelectionModel().getSelectedIndex();
    	setFile(files.get(fileIndex));
    	populate();
    	
    	}catch (Exception e) {
            e.printStackTrace();
            errorAlert(e, "Exception while changing save game information");
        } finally {
        	Platform.runLater(() -> self.setLabels());
        }
    }
    /**
     * Displays list of goods and relevant data 
     * @param event
     */
    public final void onGoods(ActionEvent event) {
        Main.showProductList();
    }
    
    private void setInterfaceEnabled(boolean isEnabled) {
        this.btnBrowseLocal.setDisable(!isEnabled);
        this.btnBrowseSave.setDisable(!isEnabled);
        this.btnBrowseMod.setDisable(!isEnabled);
        this.btnGoods.setDisable(!isEnabled);
        this.btnLoad.setDisable(!isEnabled);
        this.cbYear.setDisable(!isEnabled);

        this.tfLocalization.setDisable(!isEnabled);
        this.tfModPath.setDisable(!isEnabled);
        this.tfSaveGame.setDisable(!isEnabled);

        this.mainTable.setDisable(!isEnabled);
        this.lblPlayer.setDisable(!isEnabled);
        this.piLoad.setVisible(!isEnabled);

    }
    /**
     * Loads new save files, gives application data 
     * @param event
     */
    public final void onLoad(ActionEvent event) {

        //Main.hideProductList();
        setInterfaceEnabled(false);

        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                System.out.println();
                System.out.println("Nash: calc thread started...");
                float startTime = System.nanoTime();
                //float startTime=0;

                try {
                    reports.clear();
                    cbYear.getItems().clear();
                    String modPath = tfModPath.getText();
                    String gamePath = tfLocalization.getText();

                    PathKeeper.save();
                    //Beginnings of Relevant Code
                    if (files == null)
                    {
                    	String savePath = tfSaveGame.getText();
                    	files = new ArrayList<File>();
                    	files.add(new File(savePath));
                    	reports.add(new Report(savePath, gamePath, modPath));
                    	setFile(files.get(index));
                    }
                    else
                    {
	                    for (File file:files)
	                    {
	                    	setFile(file);
	                    	String savePath = tfSaveGame.getText();
	                    	reports.add(new Report(savePath, gamePath, modPath));
	                    }
	                    setFile(files.get(index));
                    }
                    populate();
                    reports.sort((report1,report2) -> Double.compare(report1.getYear(),report2.getYear()));
                    for (Report report: reports)
                    {
                    	cbYear.getItems().add(report.getCurrentDate());
                    	for (Country country:report.getCountryList())
                    	{
                    		int adjustedIndex = reports.indexOf(report)-1;
                    		try
                			{
                			Country oldCountry = reports.get(adjustedIndex).getCountry(country.getTag());
                				country.setGDPGrowth((float) (100.0*(country.getGdp()/oldCountry.getGdp()-1)));
                				country.setPopulationGrowth((float) (100.0*((float)country.getPopulation()/oldCountry.getPopulation()-1)));
                				country.setGDPPerCapitaGrowth((float) (100.0*(country.getGdpPerCapita()/oldCountry.getGdpPerCapita()-1)));
                				country.setConsumptionGrowth((float) (100.0*(country.getBought()/oldCountry.getBought()-1)));
                				country.setExportsGrowth((float) (100.0*(country.getExported()/oldCountry.getExported()-1)));
                				country.setImportsGrowth((float) (100.0*(country.getImported()/oldCountry.getImported()-1)));
                			}
                			catch (IndexOutOfBoundsException|NullPointerException e)
                			{
                			}
                		
                    	}
                    }
                    
                    
                    //End of Relevant Code
                    float res = ((float) System.nanoTime() - startTime) / 1000000000;
                    System.out.println("Nash: total time is " + res + " seconds");
                    Platform.runLater(() -> {
                    	index = reports.size()-1;
                    	self.setLabels();
                    	cbYear.setPromptText(reports.get(index).getCurrentDate());
                    	Main.showCharts(reports);		
                    		
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    errorAlert(e, "Exception while loading savegame");
                } finally {
                    Platform.runLater(() -> setInterfaceEnabled(true));
                }


                return 0;
            }
        };
        Thread th = new Thread(task);
        th.start();

    }
    /**
     * Populates main table and product list controller with data from current report 
     */
    private void populate()
    {
    	fillMainTable();
        productListController.setReport(reports.get(index));
        productListController.fillTable(reports.get(index).getProductList());
    }
    /**
     * Changes values of labels
     */
    private void setLabels() {
    	//int adjusted index
    	//System.out.println(reports.get(index).getCurrentDate());
        lblCurrentDate.setText(reports.get(index).getCurrentDate());
        lblPlayer.setText(reports.get(index).getPlayerCountry().getOfficialName());
        lblStartDate.setText(reports.get(index).getStartDate());
        lblPopCount.setText(reports.get(index).popCount + " pops total");
    }
    public ArrayList<Report> getReports()
    {
    	return reports;
    }
    /**
     * Opens dialog allowing for selection of save files from system
     * @param event
     */
    public final void onBrowseSave(ActionEvent event) {
        // Throws error when user cancels selection
        // SaveGame saveGame=new SaveGame();
        try {
            FileChooser fileChooser = new FileChooser();
            

            if (!PathKeeper.SAVE_PATH.isEmpty()) {
                //initialFile
                File initialFile = new File(PathKeeper.SAVE_PATH).getParentFile();
                fileChooser.setInitialDirectory(initialFile);
            }

             files = fileChooser.showOpenMultipleDialog(null);
             
             setFile(files.get(index));

        } catch (NullPointerException ignored) {
        }


    }
    /**
     * Changes save game text field to sanitized save file path
     * @param file
     */
    private void setFile(File file)
    {
    	String temp = file.getPath().replace("\\", "/"); // Usable path
        PathKeeper.SAVE_PATH = temp;
        autoChanged = true;
        tfSaveGame.setText(temp);
    }
    /**
     * Changes stored location of game files (needed for localization other data from game)
     * @param event
     */
    public final void onBrowseLocal(ActionEvent event) {
        // Throws error when user cancels selection
        // SaveGame saveGame=new SaveGame();
        try {
            DirectoryChooser dirChooser = new DirectoryChooser();
            File file;
            if (PathKeeper.LOCALISATION_PATH != null) {
                file = new File(PathKeeper.LOCALISATION_PATH);
                if (file.exists())
                    dirChooser.setInitialDirectory(file);
            }
            file = dirChooser.showDialog(null);
            String temp = file.getPath().replace("\\", "/"); // Usable path
            tfLocalization.setText(temp);
            PathKeeper.LOCALISATION_PATH = temp;
        } catch (NullPointerException ignored) {
        }


    }
    /**
     * Changes stored location of mod path (if any)- needed for localization
     * @param event
     */
    public final void onBrowseMod(ActionEvent event) {
        // Throws error when user cancels selection
        // SaveGame saveGame=new SaveGame();
        try {
            DirectoryChooser dirChooser = new DirectoryChooser();
            File file;
            if (PathKeeper.MOD_PATH != null) {
                file = new File(PathKeeper.MOD_PATH);
                if (file.exists())
                    dirChooser.setInitialDirectory(file);
            }
            file = dirChooser.showDialog(null);
            String temp = file.getPath().replace("\\", "/"); // Usable path
            tfModPath.setText(temp);
            PathKeeper.MOD_PATH = temp;
        } catch (NullPointerException ignored) {
        }
    }

    public void setProductListController(ProductListController productListController) {
        this.productListController = productListController;
    }

    private static void errorAlert(Throwable e, String text) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(text);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("The exception stacktrace was:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);

            alert.show();
        });
    }
}

class KmgConverter<T extends Number> extends StringConverter<T> {

    @Override
    public String toString(T object) {
        return Wrapper.toKMG(object);
    }

    //don't need this
    @Override
    public T fromString(String string) {
        return null;
    }
}

class PercentageConverter extends StringConverter<Float> {

    @Override
    public String toString(Float object) {
        return Wrapper.toPercentage(object);
    }

    //don't need this
    @Override
    public Float fromString(String string) {
        return null;
    }
}

class NiceFloatConverter extends StringConverter<Float> {

    @Override
    public String toString(Float object) {
        return String.format("%6.2f", object);
    }

    //don't need this
    @Override
    public Float fromString(String string) {
        return null;
    }
}