package org.victoria2.tools.vic2sgea.gui;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.chart.*;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import javafx.util.converter.*;

import org.victoria2.tools.vic2sgea.main.Country;
import org.victoria2.tools.vic2sgea.main.Report;
import org.victoria2.tools.vic2sgea.main.Wrapper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * By Anton Krylov (anthony.kryloff@gmail.com)
 * Date: 2/2/17 12:05 AM
 * <p>
 * Base controller for chart windows
 */
public class ChartsController extends BaseController {
    protected final Scene scene;
    protected final GridPane grid;
    protected final ArrayList<Report> reports;
    private int chartCount = 0;
    //Determines specific data series used in line chart 
    private String src = "";
    public ChartsController(ArrayList<Report> reports)
    {
    	this.reports = reports;
    	this.grid = new GridPane();

        //grid.setAlignment(Pos.CENTER);
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(grid);

        scene = new Scene(scrollPane, 1200, 950);
        scene.getStylesheets().add("/gui/charts.css");
    }
    public ChartsController(Report report) {
    	this.reports = new ArrayList<Report>();
        this.reports.add(report);
        this.grid = new GridPane();

        //grid.setAlignment(Pos.CENTER);
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(grid);

        scene = new Scene(scrollPane, 1200, 950);
        scene.getStylesheets().add("/gui/charts.css");

    }

    /**
     * Adds chart to grid
     *
     * @param pieChartData chart data
     * @param i            column index
     * @param j            row index
     * @param title        chart title
     * @param onEnter      function that returns caption when mouse enters an item
     * @param onClick      consumer called when an item is clicked
     * @return the created chart
     */
    protected PieChart addPieChart(List<PieChart.Data> pieChartData, int i, int j, String title,
                                Function<PieChart.Data, String> onEnter, Consumer<PieChart.Data> onClick) {
        pieChartData.sort(Comparator.comparing(PieChart.Data::getPieValue).reversed());
        final PieChart chart = new PieChart(FXCollections.observableList(pieChartData));

        chart.setStartAngle(90);
        chart.setLegendVisible(false);
        chart.setLabelsVisible(true);

        chart.setTitle(title);
        final Label caption = new Label("");
        caption.getStyleClass().add("chart-caption");

        GridPane subPane = new GridPane();
        subPane.add(chart, 0, 0);
        subPane.add(caption, 0, 1);

        grid.add(subPane, i, j);

        Double totalValue = chart.getData().stream()
                .map(PieChart.Data::getPieValue)
                .reduce(0., (d1, d2) -> d1 + d2);

        for (final PieChart.Data data : chart.getData()) {
            if (data.getPieValue() / totalValue < .001) {
                data.getNode().setVisible(false);
                continue;
            }
            data.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED,
                    e -> caption.setText(onEnter.apply(data)));
            data.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED,
                    e -> onClick.accept(data));
        }

        return chart;
    }

    /*protected PieChart addChart(List<PieChart.Data> pieChartData, String title,
                                Function<PieChart.Data, String> onEnter, Consumer<PieChart.Data> onClick) {
        int row = (chartCount / 2);
        int column = chartCount % 2;

        chartCount++;
        return addChart(pieChartData, column, row, title, onEnter, onClick);

    }*/

    protected PieChart addPieChart(List<ChartSlice> slices, String title,
                                Function<PieChart.Data, String> onEnter, Consumer<PieChart.Data> onClick) {
        int row = (chartCount / 2);
        int column = chartCount % 2;

        chartCount++;

        List<PieChart.Data> pieChartData = slices.stream()
                .map(chartSlice -> chartSlice.data)
                .collect(Collectors.toList());

        PieChart chart = addPieChart(pieChartData, column, row, title, onEnter, onClick);
        //todo ugly colors
        //slices.forEach(ChartSlice::setColor);

        return chart;

    }
	/**
	 * Returns a basic line chart with given axes and title
	 * @param xAxis
	 * @param yAxis
	 * @param title
	 * @return
	 */
    protected LineChart addLineChart(NumberAxis xAxis,NumberAxis yAxis,String title)
    {
    	LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);
    	lineChart.getYAxis().setAutoRanging(true);
    	//lineChart.getXAxis().setAutoRanging(true);
    	//lineChart.setTitle(title);
    	src = "GDP";
		
    	
    	return lineChart;
    }
    /**
     * Adds data series for a given country
     * @param lineChart
     * @param tag - tag of country
     */
    protected void addCountryToLineChart(LineChart lineChart,String tag)
    { 
    	XYChart.Series series = new XYChart.Series();
		ArrayList<XYChart.Data> data = new ArrayList<XYChart.Data>();
		ObservableList<XYChart.Data> observableData = FXCollections.observableList(data);
		for (Report report:reports)
		{
			if(report.getCountry(tag) != null)
			{
				series.setName(report.getCountry(tag).getOfficialName()+"|"+src);
				switch (src)
				{
				case "GDP":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdp()));
					break;
				case "GDP per Capita":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdpPerCapita()));
					break;
				case "GDP Percent":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdp()*100.0/report.getCountry("TOT").getGdp()));
					break;
				case "GDP Growth Rate":
					try
					{
					float old,current;
					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getGdp();
					current = reports.get(reports.indexOf(report)).getCountry(tag).getGdp();
					data.add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
					}
					catch (IndexOutOfBoundsException|NullPointerException e)
					{

					}
					break;
				case "GDP Place":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGDPPlace()));
					break;
				case "Population":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getPopulation()));
					break;
				case "Gold Income":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGoldIncome()));
					break;
				case "Factory Workforce":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getWorkforceFactory()));
					break;
				case "RGO Wokrforce":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getWorkforceRgo()));
					break;
				case "RGO Unemployment Rate":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getUnemploymentRateRgo()));
					break;
				case "Factory Unemployment Rate":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getUnemploymentRateFactory()));
					break;
				case "Size":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getSize()));
					break;
				case "Exports (Absolute)":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getExported()));
					break;
				case "Exports (Percent GDP)":
					data.add(new XYChart.Data(report.getYear(),100.0*report.getCountry(tag).getExported()/report.getCountry(tag).getGdp()));
					break;
				case "Imports (Absolute)":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getImported()));
					break;
				case "Imports (Percent GDP)":
					data.add(new XYChart.Data(report.getYear(),100.0*report.getCountry(tag).getImported()/report.getCountry(tag).getGdp()));
					break;
				case "Consumption":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getBought()));
					break;
				case "Employment":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getEmployment()));
					break;
				case "Balance of Trade (Absolute)":
					data.add(new XYChart.Data(report.getYear(),report.getCountry(tag).getExported()-report.getCountry(tag).getImported()));
					break;
				case "Balance of Trade (Relative)":
					data.add(new XYChart.Data(report.getYear(),(100.0*(report.getCountry(tag).getExported()/report.getCountry(tag).getImported()))));
					//System.out.println((100.0*(report.getCountry(tag).getExported()/report.getCountry(tag).getImported())));
					break;
				case "Balance of Trade (Percent GDP)":
					data.add(new XYChart.Data(report.getYear(),100.0*(report.getCountry(tag).getExported()-report.getCountry(tag).getImported())/report.getCountry(tag).getGdp()));
					break;
				case "Population Growth Rate":
					try
					{
					float old,current;
					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getPopulation();
					current = reports.get(reports.indexOf(report)).getCountry(tag).getPopulation();
					data.add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
					}
					catch (IndexOutOfBoundsException|NullPointerException e)
					{
					}
					break;
				case "Global Population Growth Contribution":
					try
					{
						double global,national;
						global = report.getCountry("TOT").getPopulation()-reports.get(reports.indexOf(report)-1).getCountry("TOT").getPopulation();
						national = report.getCountry(tag).getPopulation()-reports.get(reports.indexOf(report)-1).getCountry(tag).getPopulation();
						data.add(new XYChart.Data(report.getYear(),100.0*(national/global)));
						//System.out.println(national+"|"+global/national+"|"+Double.parseDouble(Float.toString(global))/Double.parseDouble(Float.toString(national)));
					}
					catch (IndexOutOfBoundsException|NullPointerException e)
					{

					}
					break;
				case "Global GDP Growth Contribution":
					try
					{
						float global,national;
						global = report.getCountry("TOT").getGdp()-reports.get(reports.indexOf(report)-1).getCountry("TOT").getGdp();
						national = report.getCountry(tag).getGdp()-reports.get(reports.indexOf(report)-1).getCountry(tag).getGdp();
						data.add(new XYChart.Data(report.getYear(),Math.abs(100.0*(national/global))));
					}
					catch (IndexOutOfBoundsException|NullPointerException e)
					{

					}
					break;
				case "GDP per Capita Growth Rate":
					try
					{
					float old,current;
					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getGdpPerCapita();
					current = reports.get(reports.indexOf(report)).getCountry(tag).getGdpPerCapita();
					data.add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
					}
					catch (IndexOutOfBoundsException|NullPointerException e)
					{

					}
					break;
				}
			}
		}
		series.setData(observableData);
		//isResizeNeeded(lineChart,((float)((XYChart.Data)series.getData().get(0)).getYValue()));
    	lineChart.getData().add(series);
    }
    /**
     * Removes data series of a country from the line chart 
     * @param lineChart
     * @param tag
     */
    protected void removeCountryFromLineChart(LineChart lineChart,String tag)
    {
    	for (XYChart.Series series:(ObservableList<XYChart.Series>)lineChart.getData())
    	{
    		for (Report report:reports)
    		{
				if (report.getCountry(tag) != null)
				{
	    			if (series.getName().equals(report.getCountry(tag).getOfficialName()+"|"+src))
	    			{
	    				lineChart.getData().remove(series);
	    				break;
	    			}
	    			else
	    			{
	    				//System.out.println(series.getName());
	    				//lineChart.System.out.println(report.getCountry(tag).getOfficialName()+"@");
	    				//System.out.println(src+"@");
	    			}
    			}
				else
				{
					//System.out.println(tag);
				}
    		}
    	}
    }
    /**
     * Expands the domain of the linechart by a year. Typically used with slider
     * The switch is necessary to remove the year for every data series on the graph
     * @param chart
     * @param year
     */
    protected void addYearToLineChart(LineChart chart, int year)
    {
    	//System.out.println("Adding Year ");
    	Platform.runLater(new Runnable() {
			@Override
			public void run()
			{
    	for (Series series: (ObservableList<Series>)chart.getData())
    	{
    		//System.out.println(series.getName());
    		for (Report report:reports)
    		{
    			if(report.getYear() == year)
    			{
    			String tag = "";
    				for (Country country:report.getCountryList())
    				{
    					if (country.getOfficialName().equals(series.getName().substring(0,series.getName().indexOf('|'))))
    					{
    						tag = country.getTag();
    						//System.out.println("success");
    					}
    				}
    				String stats = series.getName().substring(series.getName().indexOf('|')+1,series.getName().length());
    				//System.out.println("|/"+stats+"\'|");
    				//System.out.println(arg0);
    				//series.setName(report.getCountry(tag).getOfficialName()+"|"+src);
    				
    					switch (stats)
        				{
        				case "GDP":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdp()));
        					break;
        				case "GDP per Capita":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdpPerCapita()));
        					break;
        				case "GDP Percent":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGdp()*100.0/report.getCountry("TOT").getGdp()));
        					break;
        				case "GDP Growth Rate":
        					try
        					{
        					float old,current;
        					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getGdp();
        					current = reports.get(reports.indexOf(report)).getCountry(tag).getGdp();
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
        					}
        					catch (IndexOutOfBoundsException|NullPointerException e)
        					{
        						//System.out.println(e);
        					}
        					break;
        				case "GDP Place":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGDPPlace()));
        					break;
        				case "Population":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getPopulation()));
        					break;
        				case "Gold Income":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getGoldIncome()));
        					break;
        				case "Factory Workforce":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getWorkforceFactory()));
        					break;
        				case "RGO Wokrforce":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getWorkforceRgo()));
        					break;
        				case "RGO Unemployment Rate":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getUnemploymentRateRgo()));
        					break;
        				case "Factory Unemployment Rate":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getUnemploymentRateFactory()));
        					break;
        				case "Size":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getSize()));
        					break;
        				case "Exports (Absolute)":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getExported()));
        					break;
        				case "Exports (Percent GDP)":
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*report.getCountry(tag).getExported()/report.getCountry(tag).getGdp()));
        					break;
        				case "Imports (Absolute)":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getImported()));
        					break;
        				case "Imports (Percent GDP)":
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*report.getCountry(tag).getImported()/report.getCountry(tag).getGdp()));
        					break;
        				case "Consumption":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getBought()));
        					break;
        				case "Employment":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getEmployment()));
        					break;
        				case "Balance of Trade (Absolute)":
        					series.getData().add(new XYChart.Data(report.getYear(),report.getCountry(tag).getExported()-report.getCountry(tag).getImported()));
        					break;
        				case "Balance of Trade (Relative)":
        					series.getData().add(new XYChart.Data(report.getYear(),(100.0*(report.getCountry(tag).getExported()/report.getCountry(tag).getImported()))));
        					//System.out.println((100.0*(report.getCountry(tag).getExported()/report.getCountry(tag).getImported())));
        					break;
        				case "Balance of Trade (Percent GDP)":
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*(report.getCountry(tag).getExported()-report.getCountry(tag).getImported())/report.getCountry(tag).getGdp()));
        					break;
        				case "Population Growth Rate":
        					try
        					{
        					float old,current;
        					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getPopulation();
        					current = reports.get(reports.indexOf(report)).getCountry(tag).getPopulation();
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
        					}
        					catch (IndexOutOfBoundsException|NullPointerException e)
        					{
        					}
        					break;
        				case "Global Population Growth Contribution":
        					try
        					{
        						double global,national;
        						global = report.getCountry("TOT").getPopulation()-reports.get(reports.indexOf(report)-1).getCountry("TOT").getPopulation();
        						national = report.getCountry(tag).getPopulation()-reports.get(reports.indexOf(report)-1).getCountry(tag).getPopulation();
        						series.getData().add(new XYChart.Data(report.getYear(),100.0*(national/global)));
        						//System.out.println(national+"|"+global/national+"|"+Double.parseDouble(Float.toString(global))/Double.parseDouble(Float.toString(national)));
        					}
        					catch (IndexOutOfBoundsException|NullPointerException e)
        					{

        					}
        					break;
        				case "Global GDP Growth Contribution":
        					try
        					{
        						float global,national;
        						global = report.getCountry("TOT").getGdp()-reports.get(reports.indexOf(report)-1).getCountry("TOT").getGdp();
        						national = report.getCountry(tag).getGdp()-reports.get(reports.indexOf(report)-1).getCountry(tag).getGdp();
        						series.getData().add(new XYChart.Data(report.getYear(),Math.abs(100.0*(national/global))));
        					}
        					catch (IndexOutOfBoundsException|NullPointerException e)
        					{

        					}
        					break;
        				case "GDP per Capita Growth Rate":
        					try
        					{
        					float old,current;
        					old = reports.get(reports.indexOf(report)-1).getCountry(tag).getGdpPerCapita();
        					current = reports.get(reports.indexOf(report)).getCountry(tag).getGdpPerCapita();
        					series.getData().add(new XYChart.Data(report.getYear(),100.0*(current/old-1)));
        					}
        					catch (IndexOutOfBoundsException|NullPointerException e)
        					{

        					}
        					break;
        				}
    				}
    				
    			}
    		}
    	}
    	});
    }
    protected void removeYearFromLineChart(LineChart chart, int year)
    {
    	try
    	{
    	for (XYChart.Series series: (ObservableList<XYChart.Series>)chart.getData())
    	{
    		for (XYChart.Data data:(ObservableList<XYChart.Data>)series.getData())
    		{
    			if ((double)data.getXValue() == year)
    			{
    				series.getData().remove(data);
    				break;
    			}
    		}
    	}
    	}catch(ConcurrentModificationException e)
    	{
    	}
    }
    /**
     * Returns src - which determines the y-value of the latest data series on the chart
     * @return
     */
    public String getLineChartSource()
    {
    	return src;
    }
    /**
     * Changes the title and y-axis label on the chart, and the src, which determines the data series used on the chart
     * @param chart
     * @param update
     * @param isDeselected - am I adding something to the chart or removing something?
     */
    public void setLineChartSource(LineChart chart,String update,boolean isDeselected)
    {
    	src = update;
    	chart.setTitle(src);
    	chart.getYAxis().setLabel(src);
    	if (isDeselected)
    	{
    		chart.getData().clear();
    	}
    	
    	
    	
    }
    public Scene getScene() {
        return scene;
    }
    //this probably shouldn't always return 0 index,
    public Report getReport()
    {
    	return reports.get(0);
    }
    static class ChartSlice {
        private Color color;
        private PieChart.Data data;

        public ChartSlice(String name, double value, Color color) {
            this(name, value);
            this.color = color;
        }

        public ChartSlice(String name, double value) {
            data = new PieChart.Data(name, value);
        }

        void setColor() {
            if (color == null)
                return;
            String webColor = Wrapper.toWebColor(color);
            data.getNode().setStyle("-fx-pie-color: " + webColor);
        }
    }
}

